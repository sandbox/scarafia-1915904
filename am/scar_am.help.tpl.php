<?php
/**
 * @file
 * scar_am help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR AM</strong> lets you define rules for granting/denying
            admin menu access for specified accounts/roles at spedified paths.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR AM Rules:</dt>
            <dd>
                <p>
                    Format: path; rids; uids; access [//comments], where:
                    <dl>
                        <dt>path</dt>
                        <dd>
                            the path/s on which the access is to be set.
                            It is possible to use wildcards (only "*" characters
                            at the end of the path).
                        </dd>
                        <dt>rids</dt>
                        <dd>
                            a comma separated list of role ids.
                        </dd>
                        <dt>uids</dt>
                        <dd>
                            a comma separated list of user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access string. Possible values are
                            <em>grant</em>, <em>deny</em> and <em>default</em>.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // hide admin menu from everyone everywhere<br>
                    *;*;*;deny<br>
                    <br>
                    // except at home page, from users with permission<br>
                    &lt;front&gt;;;*;default<br>
                    <br>
                    // and at home page, from users 6 and 8<br>
                    &lt;front&gt;;;6,8;grant<br>
                    <br>
                    // and everywhere, from SCAR admins<br>
                    *;;a;grant<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
