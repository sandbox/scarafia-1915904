<?php
/**
 * @file
 * scar_menu help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR Menu</strong> lets you define rules for granting/denying
            access for specified accounts/roles to specified menu items.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR Menu Rules:</dt>
            <dd>
                <p>
                    Format: menu; rids; uids; access [//comments], where:
                    <dl>
                        <dt>menu</dt>
                        <dd>
                            the menu item/s on which the access is to be set.
                            Although they are similar, menu items are NOT url
                            paths. Menu items refer to the items defined in the
                            drupal's menu system. However, it is possible to use
                            wildcards (only "*" characters at the end of the
                            menu item; "%" characters are NOT wildcards, and
                            will only match "%" characters in the menu item
                            string).
                        </dd>
                        <dt>rids</dt>
                        <dd>
                            a comma separated list of role ids.
                        </dd>
                        <dt>uids</dt>
                        <dd>
                            a comma separated list of user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access string. Possible values are
                            <em>grant</em>, <em>deny</em> and <em>default</em>.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // users with roles 3 and 9, and users 22, 67 and 81 can
                    view the status report<br>
                    admin/reports/status;3,9;22,67,81;grant<br>
                    <br>
                    // users with role 4 can view all the reports<br>
                    admin/reports/*;4;;grant<br>
                    admin/repor*;4;;grant &nbsp;&nbsp;//works too<br>
                    <br>
                    // everybody can edit nodes<br>
                    node/%node/edit;*;*;grant<br>
                    <br>
                    // but user 46 must already have access<br>
                    node/%node/edit;;46;default<br>
                    <br>
                    // all SCAR administrators can access everything
                    (with comment)<br>
                    *;;a;grant &nbsp;&nbsp;//admins<br>
                    <br>
                    // nobody can access admin pages (commented with comment)<br>
                    //admin*;*;*;deny &nbsp;&nbsp;&nbsp;// ATTENTION - DANGER - CAREFUL !!!<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
