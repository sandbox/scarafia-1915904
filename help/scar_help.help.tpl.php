<?php
/**
 * @file
 * scar_help help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR Help</strong> lets you define rules for granting/denying
            access, for specified accounts/roles at spedified paths, to see
            contextual help defined by modules (such as this very text).
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR Help Rules:</dt>
            <dd>
                <p>
                    Format: path; rids; uids; access [//comments], where:
                    <dl>
                        <dt>path</dt>
                        <dd>
                            the path/s (menu items) on which the access is to be set.
                            Although they are similar, menu items are NOT url
                            paths. Menu items refer to the items defined in the
                            drupal's menu system. However, it is possible to use
                            wildcards (only "*" characters at the end of the
                            menu item; "%" characters are NOT wildcards, and
                            will only match "%" characters in the path -menu item-
                            string).
                        </dd>
                        <dt>rids</dt>
                        <dd>
                            a comma separated list of role ids.
                        </dd>
                        <dt>uids</dt>
                        <dd>
                            a comma separated list of user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access string. Possible values are
                            <em>grant</em>, <em>deny</em> and <em>default</em>.
                            So far, <em>grant</em> and <em>default</em> do the
                            same.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // hide help from everyone everywhere<br>
                    *;*;*;deny<br>
                    <br>
                    // except at administration pages, from SCAR admins.<br>
                    admin*;;a;default<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
