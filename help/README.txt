
 S C A R   -   H E L P
-----------------------

SCAR Help lets you define rules for granting/denying access, for specified
accounts/roles at spedified paths, to see contextual help defined by modules.



TO INSTALL:
-----------

- by now this (sub)module comes with the scar module together, so it is supposed
  to be already within your scar module directory (no need to download it).
- install the module (admin/build/modules).
- no need to set permissions



TO ADMINISTER:
--------------

- go to the module administration page (admin/settings/scar/help)

To do so you must to be a SCAR administrator. Out of the box, the only SCAR
Administrator UID registered is 1 (user_1). If that's you, no problem, go to the
admin page, otherwise:

1. Log in as user 1, and then set your own account as SCAR admin, or
2. If you are logged in the system as an authenticated user, go to the SCAR
   login page (scar/login) and:
        a. uncheck "Silent"
        b. enter whatever string as "User" (doesn't matter the user)
        c. enter the "Password" (out of the box, the default password is
           "admin")
   By doing so, you (your uid) will become a SCAR admin uid, or
3. If you are not logged in the system, go to the SCAR login page
   (scar/login) and:
        a. keep "Silent" checked
        b. enter A VALID user name (or UID)
        c. enter the "Password" to switch to that valid user AS A SCAR ADMIN,
           IN SILENT MODE.
   Silent mode means you are a SCAR admin ONLY FOR THAT SESSION



IN THE SCAR HELP ADMINISTRATION PAGE:
-------------------------------------

- see "Help" for instructions.
