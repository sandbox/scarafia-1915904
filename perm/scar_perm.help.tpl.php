<?php
/**
 * @file
 * scar_perm help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR Perm</strong> lets you define rules for granting
            specified <em>permissions</em> to specified accounts and/or roles.
            Unfortunately, due to drupal's access structure/design, I couldn't
            find a way to "deny" <em>permissions</em>, which would make this
            project (or project idea) even more powerful and flexible.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR Perm Rules:</dt>
            <dd>
                <p>
                    Format: perm; rids; uids; access [//comments], where:
                    <dl>
                        <dt>perm</dt>
                        <dd>
                            the permission to be set.
                        </dd>
                        <dt>rids</dt>
                        <dd>
                            a comma separated list of role ids.
                        </dd>
                        <dt>uids</dt>
                        <dd>
                            a comma separated list of user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access string. As said before,
                            only "grant" accesses make sense here.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // users with roles 3 and 9, and users 22, 67 and 81 can
                    access devel information<br>
                    access devel information;3,9;22,67,81;grant<br>
                    <br>
                    // all SCAR administrators can administer blocks
                    (with comment)<br>
                    administer blocks;;a;grant &nbsp;&nbsp;//admins<br>
                    <br>
                    // all users and groups can access content
                    (with comment)<br>
                    access content;*;*;grant &nbsp;&nbsp;//content accessible to all users<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
