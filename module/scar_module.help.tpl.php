<?php
/**
 * @file
 * scar_module help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR Module</strong> lets you define rules for
            showing/hiding/forcing status/access/actions
            for specified accounts/roles on/to specified modules. Rules defined
            here have no effect when handling modules with drush.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR Module Rules:</dt>
            <dd>
                <p>
                    Format: module; rids; uids; access [//comments], where:
                    <dl>
                        <dt>module</dt>
                        <dd>
                            the module/s on which the access/status is to be set.
                            It is possible to use wildcards (only "*" characters
                            at the end of the module).
                        </dd>
                        <dt>rids</dt>
                        <dd>
                            a comma separated list of role ids.
                        </dd>
                        <dt>uids</dt>
                        <dd>
                            a comma separated list of user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access (or action) string. Possible
                            values so far are <em>force</em>, <em>disable</em>,
                            <em>hide</em>, <em>hide_description</em>,
                            <em>hide_dependencies</em> and <em>default</em>.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // hide all devel modules from everyone<br>
                    devel*;*;*;hide<br>
                    <br>
                    // except from SCAR administrators<br>
                    devel*;*;a;default<br>
                    <br>
                    // show views module to everyone,
                    but force it to be/keep activated<br>
                    views;;*;force<br>
                    <br>
                    // show auto_nodetitle module to everyone,
                    but disable the checkbox to keep it in its current state<br>
                    auto_nodetitle;;*;disable<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
