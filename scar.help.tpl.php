<?php
/**
 * @file
 * scar help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            This is a base module. The name, <strong>SCAR</strong>, could stand
            for <em>System Control &/by Access Rules</em>, or as an acronym for
            <em>System Access Control Rules</em>, or... actually, after having
            spent over 4 days cooking my brains, searching for a good module
            name that would reflect what the module does, and seen that every
            good candidate name was already taken: <strong>AR</strong> for
            <em>Access Rules</em> taken as <strong>AR</strong> for
            <em>Arabian</em>; <strong>ACK</strong> for <em>Access Control Kit</em>
            (drupal 7); <strong>ACL</strong> for <em>Access Control Lists</em>;
            and so on... I was just too tired to keep looking. So, this is it.
        </p>
        <p>
            After reading that, you probably have a notion of what it is for.
            The idea was to be able to administer (grant/deny) access across the
            whole system, but in a more <em>private and flexible</em> way than usual.
        </p>
        <p>
            So, this module is intended to define/identify administrator/s, who
            will have an extra category (besides typical roles), and will be able
            to login as SCAR admins. Then, the idea is to write more specific
            sub modules (6 so far) in order to administer (grant/deny) more
            specific access by defining according specific
            <em><strong>access rules</strong></em>.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Admin Uids:</dt>
            <dd>
                <p>
                    Comma separated admin uids. Must be valid uids (values
                    0, 2 and not existing uids will produce error).
                </p>
            </dd>
            <dt>Admin Rids:</dt>
            <dd>
                <p>
                    Comma separated admin rids. Must be valid uids (values
                    0, 1, 2 and not existing rids will produce error).
                </p>
            </dd>
            <dt>Password:</dt>
            <dd>
                <p>
                    Enables to login (in silent mode or not) as a
                    SCAR administrator.
                    Later, at the login screen, 2 options:
                </p>
                <p>
                    1) Silent mode: you enter a valid authenticated account and
                    the SCAR pwd, and then you switch to that account, with
                    SCAR admin access in silent mode (I.e. for that session
                    only).
                </p>
                <p>
                    2) Normal mode: you must already be an authenticated user,
                    then by typing the correct pwd you become a SCAR admin.
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
