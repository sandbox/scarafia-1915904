
 S  C  A  R
------------

Base module and submodules.

The name, SCAR, could stand for System Control &/by Access Rules, or as an
acronym for System Access Control Rules, or... actually, after having spent over
4 days cooking my brains, searching for a good module name that would reflect
what the module does, and seen that every good candidate name was already taken:

AR for Access Rules taken as AR for Arabian;
ACK for Access Control Kit (drupal 7);
ACL for Access Control Lists;
and so on...

I was just too tired to keep looking. So, this is it.

After reading that, you probably have a notion of what it is for. The idea was
to be able to administer (grant/deny) access across the whole system,
but in a more private and flexible way than usual.

So, this module is intended to define/identify administrator/s, who will have an
extra category (besides typical roles), and will be able to login as
SCAR admins. Then, the idea is to write more specific sub modules (6 so far)
in order to administer (grant/deny) more specific access by defining according
specific "access rules".



TO INSTALL:
-----------

- download the module and unzip it in your modules directory
  (E.g. sites/all/modules).
- install the module (admin/build/modules).
- no need to set permissions



TO ADMINISTER:
--------------

- go to the module administration page (admin/settings/scar)

To do so you must to be a SCAR administrator. Out of the box, the only SCAR
Administrator UID registered is 1 (user_1). If that's you, no problem, go to the
admin page, otherwise:

1. Log in as user 1, and then set your own account as SCAR admin, or
2. If you are logged in the system as an authenticated user, go to the SCAR
   login page (scar/login) and:
        a. uncheck "Silent"
        b. enter whatever string as "User" (doesn't matter the user)
        c. enter the "Password" (out of the box, the default password is
           "admin")
   By doing so, you (your uid) will become a SCAR admin uid, or
3. If you are not logged in the system, go to the SCAR login page
   (scar/login) and:
        a. keep "Silent" checked
        b. enter A VALID user name (or UID)
        c. enter the "Password" to switch to that valid user AS A SCAR ADMIN,
           IN SILENT MODE.
   Silent mode means you are a SCAR admin ONLY FOR THAT SESSION



IN THE SCAR ADMINISTRATION PAGE:
--------------------------------

- see "Help" for instructions.
