<?php

/**
 * @file
 * The main module file.
 */

// =============================================================================
// DEFS
// =============================================================================
define("SCAR_USER_RULES", "//*;*;*;*;*;default\n");

// =============================================================================
// INIT
// =============================================================================

// =============================================================================
// NODE API
// =============================================================================

// =============================================================================
// USER
// =============================================================================

// =============================================================================
// MENU
// =============================================================================
/**
 * Implements hook_menu().
 */
function scar_user_menu() {
  $items = array();

  $items["admin/settings/scar/user"] = array(
    "title"             => "SCAR User",
    "description"       => "SCAR User settings",
    "type"              => MENU_LOCAL_TASK,
    "page callback"     => "drupal_get_form",
    "page arguments"    => array("_scar_user_settings_form"),
    "access callback"   => "_scar_user_settings_form_access",
    "weight"            => 50,
  );

  return $items;
}

/**
 * Access callback for SCAR User settings form.
 *
 * @return Boolean
 *   The resulting form access.
 */
function _scar_user_settings_form_access() {
  return scar_is_admin();
}

// =============================================================================
// MENU ALTER
// =============================================================================
/**
 * Implements hook_menu_alter().
 */
function scar_user_menu_alter(&$items) {
  $path = "user/%user_uid_optional";
  $ori_callback = $items[$path]["access callback"];
  $items[$path]["access callback"] = "_scar_user_view_access";
  $items[$path]["access arguments"][] = $ori_callback;
  $ori_callback = NULL;

  if (module_exists("content_profile")) {
    foreach (content_profile_get_types('names') as $type => $type_name) {
      $path = "user/%user/profile/$type";
      $ori_callback = $items[$path]["access callback"];
      $items[$path]["access callback"] = "_scar_user_profile_access";
      $items[$path]["access arguments"][] = $ori_callback;
      $ori_callback = NULL;
    }
  }

  $path = "user/%user_category/edit";
  $ori_callback = $items[$path]["access callback"];
  $items[$path]["access callback"] = "_scar_user_edit_access";
  $items[$path]["access arguments"][] = $ori_callback;
  $ori_callback = NULL;

  $path = "user/%user/delete";
  $ori_callback = $items[$path]["access callback"];
  $items[$path]["access callback"] = "_scar_user_delete_access";
  $items[$path]["access arguments"][] = $ori_callback;
  $ori_callback = NULL;
}

/**
 * Access callback for user view permission.
 *
 * @global StdClass $user
 *
 * @return Boolean
 *   The resulting access.
 */
function _scar_user_view_access() {
  global $user;

  $args = func_get_args();
  $ori_callback = array_pop($args);

  $object = $args[0];

  $access = scar_user_user_access("r", $object, $user);

  switch ($access) {
    case "deny":
      return FALSE;

    case "grant":
      return TRUE;

    default:
      if (is_bool($ori_callback)) {
        return $ori_callback;
      }
      if (empty($ori_callback)) {
        $ori_callback = "user_access";
      }
      $ori_access = call_user_func_array($ori_callback, $args);
      return $ori_access;

  }
}

/**
 * Access callback for content_profile form.
 *
 * @global StdClass $user
 *
 * @return Boolean
 *   The resulting access.
 */
function _scar_user_profile_access() {
  global $user;

  $args = func_get_args();
  $ori_callback = array_pop($args);

  $object = $args[1];

  $access = scar_user_user_access("w", $object, $user);

  switch ($access) {
    case "deny":
      return FALSE;

    case "grant":
      return TRUE;

    default:
      if (is_bool($ori_callback)) {
        return $ori_callback;
      }
      if (empty($ori_callback)) {
        $ori_callback = "user_access";
      }
      $ori_access = call_user_func_array($ori_callback, $args);
      return $ori_access;

  }
}

/**
 * Access callback for user edit form.
 *
 * @global StdClass $user
 *
 * @return Boolean
 *   The resulting access.
 */
function _scar_user_edit_access() {
  global $user;

  $args = func_get_args();
  $ori_callback = array_pop($args);

  $object = $args[0];

  $access = scar_user_user_access("w", $object, $user);

  switch ($access) {
    case "deny":
      return FALSE;

    case "grant":
      return TRUE;

    default:
      if (is_bool($ori_callback)) {
        return $ori_callback;
      }
      if (empty($ori_callback)) {
        $ori_callback = "user_access";
      }
      $ori_access = call_user_func_array($ori_callback, $args);
      return $ori_access;

  }
}

/**
 * Access callback for user delete permission.
 *
 * @global StdClass $user
 *
 * @return Boolean
 *   The resulting access.
 */
function _scar_user_delete_access() {
  global $user;

  $args = func_get_args();
  $ori_callback = array_pop($args);

  $object = $args[0];

  $access = scar_user_user_access("d", $object, $user);

  switch ($access) {
    case "deny":
      return FALSE;

    case "grant":
      return TRUE;

    default:
      if (is_bool($ori_callback)) {
        return $ori_callback;
      }
      if (empty($ori_callback)) {
        $ori_callback = "user_access";
      }
      $ori_access = call_user_func_array($ori_callback, $args);
      return $ori_access;

  }
}

/**
 * Returns access.
 *
 * Returns access for <em>subject</em> user performing <em>op</em> operation
 * on <em>object</em> user.
 *
 * @param String $op
 *   The <em>operation</em> to be performed.
 *
 * @param Mixed $object
 *   The <em>object</em> user, on which the operation is to be performed.
 *
 * @param Mixed $subject
 *   The <em>subject</em> user, which performs the operation.
 *
 * @return String
 *   The corresponding <em>access</em>.
 */
function scar_user_user_access($op, $object, $subject) {
  // Object!
  $orids = array();
  $ouid = NULL;
  $oadmin = FALSE;
  if (is_object($object) || (scar_validate_int($object) && ($object = user_load($object)))) {
    if (isset($object->uid) && isset($object->roles)) {
      $orids = array_keys($object->roles);
      $ouid = $object->uid;
      $oadmin = scar_is_admin($object);
    }
  }

  // Subject!
  $srids = array();
  $suid = NULL;
  $sadmin = FALSE;
  if (is_object($subject) || (scar_validate_int($subject) && ($subject = user_load($subject)))) {
    if (isset($subject->uid) && isset($subject->roles)) {
      $srids = array_keys($subject->roles);
      $suid = $subject->uid;
      $sadmin = scar_is_admin($subject);
    }
  }

  $self = $ouid == $suid;

  $rule = _scar_user_match_op_rule($op, $orids, $ouid, $self, $oadmin);

  $access = NULL;

  if ($sadmin && isset($rule["suid"]["a"])) {
    $access = $rule["suid"]["a"];
  }
  if (empty($access) && isset($rule["suid"][$suid])) {
    $access = $rule["suid"][$suid];
  }
  if (empty($access) && isset($rule["suid"]["*"])) {
    $access = $rule["suid"]["*"];
  }
  if (empty($access) && !empty($rule["srid"])) {
    foreach ($rule["srid"] as $srid => $value) {
      if (array_key_exists($srid, $srids)) {
        $access = $value;
      }
    }
  }
  if (empty($access) && isset($rule["srid"]["*"])) {
    $access = $rule["srid"]["*"];
  }

  return $access ? $access : "default";
}

/**
 * Returns a single rule.
 *
 * Builds and returns a sigle <em>rule</em> matching the given <em>op</em>
 * operation to be performed on an <em>object</em> account by a <em>subject</em>
 * account.
 *
 * @param String $op
 *   The operation to be performed.
 *
 * @param Array $orids
 *   The <em>object</em> account roles.
 *
 * @param Integer $ouid
 *   The <em>object</em> account uid.
 *
 * @param Boolean $self
 *   Says whether <em>object</em> and <em>subject</em> accounts are the same.
 *   If true, means to match <em>self</em> rules.
 *
 * @param Boolean $oadmin
 *   Says whether the <em>subject</em> account is an admin.
 *   If true, means to match <em>admin</em> rules.
 *
 * @return String
 *   The resulting <em>rule</em>.
 */
function _scar_user_match_op_rule($op, $orids, $ouid, $self = FALSE, $oadmin = FALSE) {
  $rule = array();

  $ops = array_merge(array("*"), array($op));
  $orids = array_merge(array("*"), $orids);
  $ouids = array_merge(array("*"), array($ouid));

  if ($self) {
    $ouids[] = "s";
  }
  if ($oadmin) {
    $ouids[] = "a";
  }

  $rules = _scar_user_load_rules();

  foreach ($orids as $orid) {
    foreach ($ops as $op) {
      if (isset($rules[$op]["orid"][$orid]["srid"])) {
        foreach ($rules[$op]["orid"][$orid]["srid"] as $srid => $access) {
          $rule["srid"][$srid] = $access;
        }
      }
      if (isset($rules[$op]["orid"][$orid]["suid"])) {
        foreach ($rules[$op]["orid"][$orid]["suid"] as $suid => $access) {
          $rule["suid"][$suid] = $access;
        }
      }
    }
  }
  foreach ($ouids as $ouid) {
    foreach ($ops as $op) {
      if (isset($rules[$op]["ouid"][$ouid]["srid"])) {
        foreach ($rules[$op]["ouid"][$ouid]["srid"] as $srid => $access) {
          $rule["srid"][$srid] = $access;
        }
      }
      if (isset($rules[$op]["ouid"][$ouid]["suid"])) {
        foreach ($rules[$op]["ouid"][$ouid]["suid"] as $suid => $access) {
          $rule["suid"][$suid] = $access;
        }
      }
    }
  }

  return $rule;
}

/**
 * Helper function.
 *
 * Loads and returns SCAR User rules.
 *
 * @staticvar Array $rules
 *
 * @param Boolean $reset
 *   Whether to reset the rules array.
 *
 * @return Array
 *   The resulting rules array.
 */
function _scar_user_load_rules($reset = FALSE) {
  static $rules;
  if ($reset) {
    $rules = NULL;
  }

  if (!isset($rules)) {
    $rules = array();
    $text = variable_get("scar_user_rules", SCAR_USER_RULES);
    $lines = scar_explode_lines($text);
    foreach ($lines as $line) {
      $rule = explode("//", $line);
      $parts = explode(';', $rule[0]);
      $ops = isset($parts[0]) ? explode(",", $parts[0]) : array();
      $orids = isset($parts[1]) ? explode(",", $parts[1]) : array();
      $ouids = isset($parts[2]) ? explode(",", $parts[2]) : array();
      $srids = isset($parts[3]) ? explode(",", $parts[3]) : array();
      $suids = isset($parts[4]) ? explode(",", $parts[4]) : array();
      $access = isset($parts[5]) ? trim($parts[5]) : NULL;
      foreach ($ops as $op) {
        if ($op = trim($op)) {
          foreach ($orids as $orid) {
            if ($orid = trim($orid)) {
              foreach ($srids as $srid) {
                if ($srid = trim($srid)) {
                  $rules[$op]["orid"][$orid]["srid"][$srid] = $access;
                }
              }
              foreach ($suids as $suid) {
                if ($suid = trim($suid)) {
                  $rules[$op]["orid"][$orid]["suid"][$suid] = $access;
                }
              }
            }
          }
          foreach ($ouids as $tuid) {
            if ($tuid = trim($tuid)) {
              foreach ($srids as $srid) {
                if ($srid = trim($srid)) {
                  $rules[$op]["ouid"][$tuid]["srid"][$srid] = $access;
                }
              }
              foreach ($suids as $suid) {
                if ($suid = trim($suid)) {
                  $rules[$op]["ouid"][$tuid]["suid"][$suid] = $access;
                }
              }
            }
          }
        }
      }
    }
  }
  return $rules;
}

// =============================================================================
// FORM
// =============================================================================
/**
 * Builds SCAR User settings form.
 *
 * @return Array
 *   The resulting form array.
 */
function _scar_user_settings_form() {
  $form = array();

  $form["scar_user_version"] = array(
    "#type"             => "textfield",
    "#title"            => t("Version"),
    "#description"      => t("SCAR User Version"),
    "#default_value"    => variable_get("scar_user_version", ''),
    "#size"             => 32,
    "#maxlength"        => 32,
    "#required"         => FALSE,
    "#attributes"       => array("readonly" => "readonly"),
    "#prefix"           => "<div style=\"float: left;\">",
    "#suffix"           => "</div>",
  );

  $form["scar_user_weight"] = array(
    "#type"             => "textfield",
    "#title"            => t("Weight"),
    "#description"      => t("SCAR User module weight"),
    "#default_value"    => variable_get("scar_user_weight", ''),
    "#size"             => 4,
    "#maxlength"        => 4,
    "#required"         => FALSE,
    "#prefix"           => "<div style=\"float: left; margin-left: 20px;\">",
    "#suffix"           => "</div>",
  );

  $form["scar_user_rules"] = array(
    "#type"             => "textarea",
    "#title"            => t("SCAR User Rules"),
    "#description"      => t("Format: op; orids; ouids; srids; suids; access [//comments]"),
    "#default_value"    => variable_get("scar_user_rules", SCAR_USER_RULES),
    "#rows"             => 10,
    "#required"         => FALSE,
    "#attributes"       => array("style" => "font-family: monospace;"),
    "#prefix"           => "<div style=\"clear: both;\">",
    "#suffix"           => "</div>",
  );

  $form["submit"] = array(
    "#type"             => "submit",
    "#value"            => t("Save settings"),
  );

  drupal_set_title("SCAR User Settings");
  return $form;
}

/**
 * Handles SCAR User settings form validation.
 *
 * @param Array $form
 *   The form array.
 *
 * @param Array $form_state
 *   The form state array.
 */
function _scar_user_settings_form_validate(&$form, &$form_state) {
  $values = $form_state["values"];
  $weight = $values["scar_user_weight"];
  if (!scar_validate_int($weight)) {
    form_set_error("scar_user_weight", "Weight is not valid");
  }
}

/**
 * Handles SCAR User settings form submission.
 *
 * @param Array $form
 *   The form array.
 *
 * @param Array $form_state
 *   The form state array.
 */
function _scar_user_settings_form_submit(&$form, &$form_state) {
  $values = $form_state["values"];
  $rules = $values["scar_user_rules"];
  variable_set("scar_user_rules", $rules);
  $weight = $values["scar_user_weight"];
  db_query("UPDATE {system} SET weight = %d WHERE type = 'module' AND name = '%s'", $weight, "scar_user");
  variable_set("scar_user_weight", $weight);
}

// =============================================================================
// FORM ALTER
// =============================================================================
/**
 * Implements hook_form_FORM_ID_alter for user_profile_form().
 */
function scar_user_form_user_profile_form_alter(&$form, &$form_state) {
  $form["#after_build"][] = "_scar_user_after_build_user_profile_form";
}

/**
 * Helper function.
 *
 * Sets user profile form.
 *
 * @param Array $form
 *   The form array.
 *
 * @param Array $form_state
 *   The form state array.
 *
 * @return Array
 *   The resulting form array.
 */
function _scar_user_after_build_user_profile_form(&$form, &$form_state) {
  global $user;
  $object = $form["_account"]["#value"];

  $access = scar_user_user_access("d", $object, $user);
  if ($access == "deny") {
    unset($form["delete"]);
  }

  return $form;
}

// =============================================================================
// PREPROCESS
// =============================================================================

// =============================================================================
// HELP
// =============================================================================
/**
 * Implements hook_help().
 */
function scar_user_help($path, $arg) {
  switch ($path) {
    case "admin/settings/scar/user":
      drupal_add_js("misc/collapse.js");
      $legend = t("Help");
      ob_start();
      include "scar_user.help.tpl.php";
      $help = ob_get_contents();
      ob_clean();
      return $help;

  }
}

// =============================================================================
