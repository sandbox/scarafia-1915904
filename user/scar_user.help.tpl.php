<?php
/**
 * @file
 * scar_user help tpl.
 */
?>
<fieldset class="collapsible collapsed">
    <legend><?php print $legend; ?></legend>
    <div>
        <p>
            <strong>SCAR User</strong> lets you define rules for granting/denying
            access to perform specified operations, by <em>subject</em> accounts
            to <em>object</em> accounts.
        </p>
        <p>
            In this screen:
        </p>
        <dl>
            <dt>Weight:</dt>
            <dd>
                <p>
                    The module weight (system table). Tells the bootstrap system
                    in which order the modules (and their hooks) shall be loaded.
                    "Lighter" modules are loaded before "heavier" modules.
                </p>
            </dd>
            <dt>SCAR User Rules:</dt>
            <dd>
                <p>
                    Format: op; orids; ouids; srids; suids; access [//comments],
                    where:
                    <dl>
                        <dt>op</dt>
                        <dd>
                            the <em>operation</em> to be performed. Possible
                            values are
                            <em>r (read/view)</em>,
                            <em>w (write/edit/update)</em>,
                            <em>d (delete)</em>,
                            <em>l (list)</em>,
                        </dd>
                        <dt>orids</dt>
                        <dd>
                            a comma separated list of <em>"object"</em> role ids.
                        </dd>
                        <dt>ouids</dt>
                        <dd>
                            a comma separated list of <em>"object"</em> user ids.
                        </dd>
                        <dt>srids</dt>
                        <dd>
                            a comma separated list of <em>"subject"</em> role ids.
                        </dd>
                        <dt>suids</dt>
                        <dd>
                            a comma separated list of <em>"subject"</em> user ids.
                        </dd>
                        <dt>access</dt>
                        <dd>
                            the correspondig access string. Possible values are
                            <em>grant</em>, <em>deny</em> and <em>default</em>.
                        </dd>
                    </dl>
                    Examples:
                </p>
                <p style="margin-left: 20px; font-family: monospace">
                    // commented line; will be ignored <br>
                    // whatever<br>
                    <br>
                    // users with roles 4 and/or 5 cannot be edited nor deleted<br>
                    w,d;4,5;;*;*;deny<br>
                    w,d;4,5;;;*;deny // works too<br>
                    w,d;4,5;;*;;deny // works too<br>
                    <br>
                    // except by users 16, 25 and 124<br>
                    w,d;4,5;;;16,25,124;grant<br>
                    <br>
                    // and them selves<br>
                    w,d;;s;4,5;;grant<br>
                </p>
            </dd>
        </dl>
    </div>
</fieldset>
