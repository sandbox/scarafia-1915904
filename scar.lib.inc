<?php

/**
 * @file
 * A lib file with functions.
 */

/**
 * Validates an integer value.
 *
 * @param Mixed $val
 *   The value to validate.
 *
 * @return Boolean
 *   The validation result.
 */
function scar_validate_int($val) {
  if ($val === NULL) {
    return FALSE;
  }
  return ($val !== TRUE) && ((string) (int) $val) == ((string) $val);
}

/**
 * Validates a decimal value.
 *
 * @param Mixed $val
 *   The value to validate.
 *
 * @return Boolean
 *   The validation result.
 */
function scar_validate_decimal($val) {
  if ($val === NULL) {
    return FALSE;
  }
  return ($val !== TRUE) && ((string) (double) $val) == ((string) $val);
}

/**
 * Validates a numeric value.
 *
 * @param Mixed $val
 *   The value to validate.
 *
 * @return Boolean
 *   The validation result.
 */
function scar_validate_number($val) {
  return (scar_validate_int($val) || scar_validate_decimal($val));
}

/**
 * Explodes into integers array.
 *
 * Explodes a string into an integers array, according to a given (or default)
 * delimiter.
 *
 * @param String $string
 *   The string to explode.
 *
 * @param String $delimiter
 *   The optional delimiter.
 *
 * @return Array
 *   The resulting array.
 */
function scar_explode_ints($string, $delimiter = ",") {
  $ints = array();

  $aux = explode($delimiter, $string);
  foreach ($aux as $int) {
    $int = trim($int);
    if (scar_validate_int($int)) {
      $ints[] = (int) $int;
    }
  }

  return $ints;
}

/**
 * Explodes into lines array.
 *
 * Explodes a given text into an array containing the lines.
 *
 * @param String $text
 *   The text to explode.
 *
 * @param String $delimiter
 *   The optional delimiter.
 *
 * @return Array
 *   The resulting array.
 */
function scar_explode_lines($text, $delimiter = "\n") {
  $lines = explode($delimiter, $text);
  if (empty($lines[count($lines) - 1])) {
    array_pop($lines);
  }
  return $lines;
}
